package com.beauto.attendance;

import static org.hamcrest.CoreMatchers.containsString;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.springframework.http.HttpStatus;
import java.io.File;
import java.time.LocalDateTime;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import com.beauto.attendance.AttendanceApplication;
import com.beauto.attendance.model.EmployeeModel;
import com.beauto.attendance.repository.EmployeeRepo;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.builder.MultiPartSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import static com.jayway.restassured.RestAssured.given;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class AttendanceApplicationTests {

	@Value("${local.server.port}")
	protected int serverPort;

	@Before
	public void setUp() {
		RestAssured.port = serverPort;
	}

	@Autowired
	EmployeeRepo employeeRepo;

	@Test
	public void fileuploadTest() {
		given().multiPart(new MultiPartSpecBuilder(new File("/home/nikhil/Downloads/SampleDataForAtt.csv")).fileName("")
				.controlName("attendanceSheet").mimeType("text/csv").build()).when().post("/uploadFile").then()
				.statusCode(equalTo(HttpStatus.OK.value()));
	}

	@Test
	@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
	public void employeeInsertTest() {
		String name = "Nikhil";
		long id = 0;
		LocalDateTime local = LocalDateTime.now();
		Integer status = 1;
		EmployeeModel employee = new EmployeeModel(name, local, status);
		Response res = given().body(employee).contentType(ContentType.JSON).when().post("/api/create");

		res.then().statusCode(equalTo(HttpStatus.OK.value())).body("id", not(nullValue())).body("name", is(name))
				.body("status", is(status));

		JsonPath jspath = res.jsonPath();
		id = jspath.getLong("id");

		Optional<EmployeeModel> employeeOnRecord = employeeRepo.findById(id);

		assertEquals(name, employeeOnRecord.get().getName());
	}

	@Test
	@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
	public void employeeUpdateTest() {
		String name = "Nikhil";
		long id = 1;
		LocalDateTime local = LocalDateTime.now();
		Integer status = 1;
		EmployeeModel employee = new EmployeeModel(name, local, status);
		employeeRepo.save(employee);
		Optional<EmployeeModel> userRecord = employeeRepo.findByName(name);
		System.out.println("user created : " + userRecord.get().toString());
		long idFetch = userRecord.get().getId();
		String updatedName = "Aamir";
		employee.setName(updatedName);
		System.out.println(employee.toString());
		given().body(employee).contentType(ContentType.JSON).when().put("/api/update/" + idFetch).then()
				.statusCode(equalTo(HttpStatus.OK.value())).body("id", not(nullValue())).body("status", is(status))
				.body("name", is(updatedName));
		userRecord = employeeRepo.findByName(updatedName);
		System.out.println("++++++++++++++++++++++++" + userRecord.toString());
		assertEquals(updatedName, userRecord.get().getName());
	}

	@Test
	public void getByIdTest() {
		String name = "Nikhil";
		LocalDateTime local = LocalDateTime.now();
		Integer status = 1;
		EmployeeModel employee = new EmployeeModel(name, local, status);
		employeeRepo.save(employee);
		long id = employee.getId();
		given().body(employee).contentType(ContentType.JSON).when().get("/api/find/" + id).then()
				.statusCode(equalTo(HttpStatus.OK.value()));
	}

	@Test
	@DirtiesContext(classMode = ClassMode.AFTER_EACH_TEST_METHOD)
	public void deleteByIdTest() {
		String name = "Nikhil";
		LocalDateTime local = LocalDateTime.now();
		Integer status = 1;
		EmployeeModel employee = new EmployeeModel(name, local, status);
		employeeRepo.save(employee);
		long id = employee.getId();
		given().body(employee).contentType(ContentType.JSON).when().delete("/api/delete/" + id).then()
				.statusCode(equalTo(HttpStatus.OK.value()));
	}
	
}
