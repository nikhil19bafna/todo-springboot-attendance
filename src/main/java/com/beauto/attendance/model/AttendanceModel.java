package com.beauto.attendance.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class AttendanceModel {
	@Id
	@GeneratedValue
	private Long id;
	private Long employeeId;
	private LocalDateTime dateOfAttendance;
	private LocalDateTime checkInTime;
	private LocalDateTime lunchTimeOut;
	private LocalDateTime checkOutTime;
	private LocalDateTime lunchTimeIn;
	private Integer status;

}
