package com.beauto.attendance.model;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
public class EmployeeModel {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	private String name;
	private LocalDateTime joiningDate;
	private int status;

	public EmployeeModel(String name, LocalDateTime joiningDate, int status) {
		this.name = name;
		this.joiningDate = joiningDate;
		this.status = status;
	}

}
