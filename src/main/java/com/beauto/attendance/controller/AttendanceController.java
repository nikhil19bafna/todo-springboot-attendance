package com.beauto.attendance.controller;

import java.io.FileReader;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.beauto.attendance.model.AttendanceModel;
import com.beauto.attendance.repository.AttendanceRepo;

import com.opencsv.CSVReader;

@RestController
public class AttendanceController {

	@Autowired
	AttendanceRepo attendanceRepo;

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST, consumes = { "multipart/form-data" })
	@ResponseBody
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile[] files) {
			try {
			AttendanceModel attendance = new AttendanceModel();
			String csvFile = "/home/nikhil/snap/skype/common/SampleDataForAtt.csv";
			CSVReader reader = null;
			reader = new CSVReader(new FileReader(csvFile));
			String[] values;
			values = reader.readNext();
			while ((values = reader.readNext()) != null) {
				attendance.setId(Long.parseLong(values[0].trim()));
				attendance.setEmployeeId(Long.parseLong(values[1].trim()));
				attendance.setDateOfAttendance(LocalDateTime.parse(values[2].replace(" ", "")));
				attendance.setCheckInTime(LocalDateTime.parse(values[3].replace(" ", "")));
				attendance.setLunchTimeOut(LocalDateTime.parse(values[4].replace(" ", "")));
				attendance.setLunchTimeIn(LocalDateTime.parse(values[5].replace(" ", "")));
				attendance.setCheckOutTime(LocalDateTime.parse(values[6].replace(" ", "")));
				attendance.setStatus(Integer.parseInt(values[7].trim()));
				attendanceRepo.save(attendance);
			}
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
