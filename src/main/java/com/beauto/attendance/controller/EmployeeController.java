package com.beauto.attendance.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.beauto.attendance.model.EmployeeModel;
import com.beauto.attendance.repository.EmployeeRepo;

@RestController
@RequestMapping(value = "/api")
public class EmployeeController {

	@Autowired
	EmployeeRepo employeeRepo;

	@RequestMapping(value = "/create", method = RequestMethod.POST)
	public EmployeeModel createEmployee(@RequestBody EmployeeModel emp) {
		EmployeeModel empl = employeeRepo.save(emp);
		return empl;
	}

	@RequestMapping(value = "/update/{id}", method = RequestMethod.PUT)
	public EmployeeModel updateEmployee(@RequestBody EmployeeModel emp, @PathVariable long id) throws Exception {
		EmployeeModel userResponse = employeeRepo.findById(id).orElseThrow(Exception::new);
		userResponse.setName(emp.getName());
		userResponse.setStatus(emp.getStatus());
		return employeeRepo.save(userResponse);
	}

	@RequestMapping(value = "/find/{id}", method = RequestMethod.GET)
	public Optional<EmployeeModel> getById(@PathVariable long id) {
		Optional<EmployeeModel> emo = employeeRepo.findById(id);
		return emo;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	public void deleteById(@PathVariable long id) throws Exception {
		EmployeeModel userResponse = employeeRepo.findById(id).orElseThrow(Exception::new);
		employeeRepo.delete(userResponse);
	}

	@RequestMapping(value = "/findAll", method = RequestMethod.GET)
	public Iterable<EmployeeModel> findAallEmployee() {
		Iterable<EmployeeModel> e = employeeRepo.findAll();
		e.forEach(name -> {
		    System.out.println(name);
		});
		return e;
	}

}
