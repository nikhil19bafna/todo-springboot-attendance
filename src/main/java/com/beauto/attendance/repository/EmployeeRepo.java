package com.beauto.attendance.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.beauto.attendance.model.EmployeeModel;

@Repository
public interface EmployeeRepo extends CrudRepository<EmployeeModel, Long> {

	Optional<EmployeeModel> findByName(String name);

}
