package com.beauto.attendance.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.beauto.attendance.model.AttendanceModel;

@Repository
public interface AttendanceRepo extends CrudRepository<AttendanceModel, Long>{
	
    
	
}
